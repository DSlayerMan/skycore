/*
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AchievementMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "culling_of_stratholme.h"

enum Spells
{
    SPELL_CORRUPTING_BLIGHT                     = 60588,
    SPELL_VOID_STRIKE                           = 60590
};

enum Yells
{
    SAY_AGGRO                                   = 0,
    SAY_DEATH                                   = 1,
    SAY_FAIL                                    = 2
};

class boss_infinite_corruptor : public CreatureScript
{
public:
    boss_infinite_corruptor() : CreatureScript("boss_infinite_corruptor") { }

    CreatureAI* GetAI(Creature* creature) const OVERRIDE
    {
        return GetInstanceAI<boss_infinite_corruptorAI>(creature);
    }

    struct boss_infinite_corruptorAI : public ScriptedAI
    {
        boss_infinite_corruptorAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;

        uint32 m_VoidStrikeTimer;
        uint32 m_CorruptingBlight;

        void Reset() OVERRIDE
        {
            if (instance)
            {
                instance->SetData(DATA_INFINITE_EVENT, NOT_STARTED);
            }

            m_CorruptingBlight = 8000;
            m_VoidStrikeTimer  = urand(1000, 3000);
        }

        void EnterCombat(Unit* /*who*/) OVERRIDE
        {
            Talk(SAY_AGGRO);
            if (instance)
                instance->SetData(DATA_INFINITE_EVENT, IN_PROGRESS);
        }

        void UpdateAI(uint32 uiDiff) OVERRIDE
        {

            //Return since we have no target
            if (!UpdateVictim())
                return;

            if (m_VoidStrikeTimer <= uiDiff)
            {
                DoCastVictim(SPELL_VOID_STRIKE);
                m_VoidStrikeTimer = urand(1000, 3000);
            }
            else m_VoidStrikeTimer -= uiDiff;

            if (m_CorruptingBlight <= uiDiff)
            {
               if (Unit *pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0))
                        DoCast(pTarget, SPELL_CORRUPTING_BLIGHT);
                m_CorruptingBlight = 8000;
            }
            else m_CorruptingBlight -= uiDiff;

            DoMeleeAttackIfReady();
        }

        void JustDied(Unit* /*killer*/) OVERRIDE
        {
            Talk(SAY_DEATH);
            if (instance)
                instance->SetData(DATA_INFINITE_EVENT, DONE);

            instance->DoUpdateWorldState(WORLDSTATE_TIME_GUARDIAN_SHOW, 0);
            //now, award achievement
            uint32 achievement_id = 1817;

            Map::PlayerList const& players = instance->instance->GetPlayers();
    
            if (!players.isEmpty())
            {
                for (Map::PlayerList::const_iterator itr = players.begin(); itr != players.end(); ++itr)
                {
                    if (Player* player = itr->GetSource())
                    {
                        if(!player->HasAchieved(achievement_id))
                            if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(achievement_id))
                                player->CompletedAchievement(achievementEntry);

                        if(player->HasAura(SPELL_CORRUPTING_BLIGHT))
                            player->RemoveAurasDueToSpell(SPELL_CORRUPTING_BLIGHT);
                    }
                }
            }
        }
    };

};

void AddSC_boss_infinite_corruptor()
{
    new boss_infinite_corruptor();
}
