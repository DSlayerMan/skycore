#include "ScriptPCH.h"
#include "Chat.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "Pet.h"
#include "Player.h"
#include "ReputationMgr.h"
#include "ScriptMgr.h"
#include "vote.h"

enum StatusCodes
{
	STATUS_OPEN = 1,
	STATUS_APPROVED = 2,
	STATUS_DONE = 3,
	STATUS_DECLINED = 4,
};

/*
enum SpecialSpells
{
	SPELL_EXPERT_RIDING = 34090,
	SPELL_COLD_WEATHER_FLY = 54197,
	SPELL_DRUID_BEAR_FORM = 5487,
	SPELL_WARLOCK_SUMMON_VOIDWALKER = 697,
	SPELL_WARLOCK_SUMMON_SUCCUBUS = 712,
	SPELL_WARLOCK_SUMMON_FELHUNTER = 691,
	SPELL_WARRIOR_BERSERKER_STANCE = 2458,
	SPELL_WARRIOR_DEFENSIVE_STANCE = 71,
	SPELL_ROGUE_MIX_VENOM = ,
	SPELL_PALADIN_RESSURECT = ,
	SPELL_HUNTER_REVIVE_PET =,
	SPELL_HUNTER_CHARM_PET =,
	SPELL_HUNTER_FEED_PET =,
	SPELL_HUNTER_FREE_PET =,
	SPELL_SHAMAN_FIRE_TOTEM =,
	SPELL_SHAMAN_WATER_TOTEM =,
	SPELL_SHAMAN_EARTH_TOTEM =,
	SPELL_SHAMAN_AIR_TOTEM =,
}
*/
class transfer_commands : public CommandScript
{
public:
	transfer_commands() : CommandScript("transfer_commands") {}

	ChatCommand* GetCommands() const 
	{

		static ChatCommand transferSecCommandTable[] =
        {
            { "test",          	rbac::RBAC_PERM_COMMAND_LEVELUP,          	true,	&HandleTransferTestCommand,	"", 	NULL },
            { "do",         	rbac::RBAC_PERM_COMMAND_LEVELUP,          	true,  	&HandleTransferDoCommand,	"", 	NULL },
            { NULL,             0,											false, 	NULL,                       "", 	NULL }
        };

		static ChatCommand transferCommandTable[]=
		{
			{ "transfer",      	rbac::RBAC_PERM_COMMAND_LEVELUP,			true,  	NULL,              			"",  	transferSecCommandTable },
            { NULL,             0,											false, 	NULL,                       "", 	NULL }
		};

		return transferCommandTable;
	}

	static bool HandleTransferTestCommand(ChatHandler* handler, const char* args)
	{
		Player* target = handler->getSelectedPlayer();
		//Test if we have a target
		if(!target)
		{
			handler->PSendSysMessage("Niemanden anvisiert. Breche ab!");
			return true;
		}

		//char* transfer_number = (char*)args;
		uint32 transfer_number = atoi(args);

		if(!transfer_number)
			return false;

		handler->PSendSysMessage("Transfer ausgewählt: %u", transfer_number);

		//get transfer from DB

		PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_TRANSFER);
		stmt->setUInt32(0, transfer_number);
		PreparedQueryResult result = LoginDatabase.Query(stmt);

		if(result)
		{
			Field* fields = result->Fetch();
			//Test on Account:
			uint32 accountId = target->GetSession()->GetAccountId();
			if(accountId != fields[0].GetUInt32())
			{
				handler->PSendSysMessage("Falscher Account! Transfer gehört zu Account %u, anstatt zu %u. Breche ab!",fields[0].GetUInt32(), accountId);
				return true;
			}
			//Prepare spell & factions!

			SpellInfo const* first_profession_spell = sSpellMgr->GetSpellInfo(fields[6].GetUInt32());
			uint32 first_profession_value = fields[7].GetUInt32();
			SpellInfo const* second_profession_spell = sSpellMgr->GetSpellInfo(fields[8].GetUInt32());
			uint32 second_profession_value = fields[9].GetUInt32();

			FactionEntry const* first_faction = sFactionStore.LookupEntry(fields[10].GetUInt32());
			uint32 first_faction_value = fields[11].GetUInt32();
			FactionEntry const* second_faction = sFactionStore.LookupEntry(fields[12].GetUInt32());
			uint32 second_faction_value = fields[13].GetUInt32();

			uint8 status_code = fields[14].GetUInt8();


			std::string comment = fields[15].GetString();

			int locale = handler->GetSessionDbcLocale();

        	handler->SendSysMessage("Vergleich des angewählten Charakters mit dem Transfer ergab:");
        	handler->PSendSysMessage("Name: %s <-> %s", target->GetName().c_str(), fields[1].GetString().c_str() );
        	handler->PSendSysMessage("Level: %u <-> %u", target->getLevel(), fields[2].GetUInt32() );
			handler->PSendSysMessage("Rasse: %u <-> %u", target->getRace(), fields[4].GetUInt32() );
			handler->PSendSysMessage("Klasse: %u <-> %u", target->getClass(), fields[5].GetUInt32());
			if(first_profession_spell)
				handler->PSendSysMessage("Gewünschter erster Beruf: %s auf %u", first_profession_spell->SpellName[locale], first_profession_value);
			if(second_profession_spell)
				handler->PSendSysMessage("Gewünschter zweiter Beruf: %s auf %u", second_profession_spell->SpellName[locale], second_profession_value);
			if(first_faction)
				handler->PSendSysMessage("Gewünschte erste Fraktion: %s auf %u", first_faction->name[locale], first_faction_value);
			if(second_faction)
				handler->PSendSysMessage("Gewünschte zweite Fraktion: %s auf %u", second_faction->name[locale], second_faction_value);
			handler->PSendSysMessage("Status des Transfers: %s", getStatusToCode(status_code).c_str());
			handler->PSendSysMessage("Kommentar des Gamemasters: %s", comment.c_str());
		}		
		else
		{
			handler->SendSysMessage("Transfer konnte nicht gefunden werden!");
			handler->SendSysMessage("Bitte gib einen gültigen Transfer an!");
		}
		return true; 
	}
	static bool HandleTransferDoCommand(ChatHandler* handler, const char* args)
	{
		Player* target = handler->getSelectedPlayer();
		//Test if we have a target
		if(!target)
		{
			handler->PSendSysMessage("Nichts anvisiert. Breche ab!");
			return true;
		}

		//char* transfer_number = (char*)args;
		uint32 transfer_number = atoi(args);

		if(!transfer_number)
			return true;

		handler->PSendSysMessage("Transfer ausgewählt: %u", transfer_number);

		//get transfer from DB

		PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_TRANSFER);
		stmt->setUInt32(0, transfer_number);
		PreparedQueryResult result = LoginDatabase.Query(stmt);

		if(result)
		{
			Field* fields = result->Fetch();
			//Test on Account:
			uint32 accountId = target->GetSession()->GetAccountId();
			if(accountId != fields[0].GetUInt32())
			{
				handler->PSendSysMessage("Falscher Account! Transfer gehört zu Account %u, anstatt zu %u. Breche ab!",fields[0].GetUInt32(), accountId);
				return false;
			}

			//check, if account has less than 3 transfers:

			PreparedStatement* count_stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_TRANSFER_COUNT);
			count_stmt->setUInt32(0, accountId);
			PreparedQueryResult count_result = LoginDatabase.Query(count_stmt);

			if(result)
			{
				Field* count = count_result->Fetch();
				if(count[0].GetUInt32() > 3)
				{
					handler->PSendSysMessage("Zu viele Transfers. Dieser Account hat bereits %u Transfers druchgeführt",count[0].GetUInt32());
					return true;
				}
			}

			//Everything ok:
			//Prepare spell & factions!

			SpellInfo const* first_profession_spell = sSpellMgr->GetSpellInfo(fields[6].GetUInt32());
			uint32 first_profession_value = fields[7].GetUInt32();
			SpellInfo const* second_profession_spell = sSpellMgr->GetSpellInfo(fields[8].GetUInt32());
			uint32 second_profession_value = fields[9].GetUInt32();

			FactionEntry const* first_faction = sFactionStore.LookupEntry(fields[10].GetUInt32());
			uint32 first_faction_value = fields[11].GetUInt32();
			FactionEntry const* second_faction = sFactionStore.LookupEntry(fields[12].GetUInt32());
			uint32 second_faction_value = fields[13].GetUInt32();

			uint8 status_code = fields[14].GetUInt8();

			if(status_code != STATUS_APPROVED)
			{
				handler->SendSysMessage("Der ausgewählte Transfer ist nicht bestätigt. Unbestätigte Transfers können nicht durchgeführt werden!");
				return true;
			}

			std::string comment = fields[15].GetString();

			int locale = handler->GetSessionDbcLocale();

        	handler->SendSysMessage("Vergleich des angewählten Charakters mit dem Transfer ergab:");
        	handler->PSendSysMessage("Name: %s <-> %s", target->GetName().c_str(), fields[1].GetString().c_str() );
        	handler->PSendSysMessage("Level: %u <-> %u", target->getLevel(), fields[2].GetUInt32() );
			handler->PSendSysMessage("Rasse: %u <-> %u", target->getRace(), fields[4].GetUInt32() );
			handler->PSendSysMessage("Klasse: %u <-> %u", target->getClass(), fields[5].GetUInt32());
			if(first_profession_spell)
				handler->PSendSysMessage("Gewünschter erster Beruf: %s auf %u", first_profession_spell->SpellName[locale], first_profession_value);
			if(second_profession_spell)
				handler->PSendSysMessage("Gewünschter zweiter Beruf: %s auf %u", second_profession_spell->SpellName[locale], second_profession_value);
			if(first_faction)
				handler->PSendSysMessage("Gewünschte erste Fraktion: %s auf %u", first_faction->name[locale], first_faction_value);
			if(second_faction)
				handler->PSendSysMessage("Gewünschte zweite Fraktion: %s auf %u", second_faction->name[locale], second_faction_value);
			handler->PSendSysMessage("Status des Transfers: %s", getStatusToCode(status_code).c_str());
			handler->PSendSysMessage("Kommentar des Gamemasters: %s", comment.c_str());
			unlearnAllProfessions(target);
			handler->PSendSysMessage("Dem Ziel wurden alle bereits gelernten Berufe verlernt.");
			ChatHandler target_chat_handler = ChatHandler(target->GetSession());

			//Now start Transfer:
			target_chat_handler.PSendSysMessage("Dein Charakter wird nun auf den Stand von Transfer #%u angepasst.", transfer_number);
			target_chat_handler.PSendSysMessage("Deine Berufe wurden wegen des Transfers verlernt.");

			//Level Player Up:
			target->GiveLevel(fields[2].GetUInt32());
        	target->InitTalentForLevel();
        	target->SetUInt32Value(PLAYER_XP, 0);
        	target_chat_handler.PSendSysMessage(LANG_YOURS_LEVEL_UP, handler->GetNameLink().c_str(), 80);

        	//Teach both skills:
        	if(first_profession_spell)
        	{
        		target->learnSpell(first_profession_spell->Id, false);
        		uint32 first_skill = getSkilltoLearningSpell(first_profession_spell->Id);
        		target->SetSkill(first_skill, target->GetSkillStep(first_skill), first_profession_value, target->GetPureMaxSkillValue(first_skill));
        	}
        	if(second_profession_spell)
        	{
        		target->learnSpell(second_profession_spell->Id, false);
        		uint32 second_skill = getSkilltoLearningSpell(second_profession_spell->Id);
        		target->SetSkill(second_skill, target->GetSkillStep(second_skill), first_profession_value, target->GetPureMaxSkillValue(second_skill));
        	}        		

        	//Set factions to desired values:
        	if(first_faction)
        	{
        		target->GetReputationMgr().SetOneFactionReputation(first_faction, first_faction_value, false);
        		target->GetReputationMgr().SendState(target->GetReputationMgr().GetState(first_faction));
        	}

        	if(second_faction)
        	{
        		target->GetReputationMgr().SetOneFactionReputation(second_faction, second_faction_value, false);
        		target->GetReputationMgr().SendState(target->GetReputationMgr().GetState(second_faction));
        	}
 			

        	AddPlayerSuitableWeaponSkills(target);

        	vote_functions::SendItemviaMail(target, "Glücksbringer vergangener Zeiten", "Seht euch nach einem Händler hierfür in Dalaran um, um eure Rüstung wieder zu erlangen", 37711);
        	vote_functions::SendItemviaMail(target, "Glücksbringer vergangener Zeiten", "Seht euch nach einem Händler hierfür in Dalaran um, um eure Rüstung wieder zu erlangen", 37711);
        	vote_functions::SendItemviaMail(target, "Glücksbringer vergangener Zeiten", "Seht euch nach einem Händler hierfür in Dalaran um, um eure Rüstung wieder zu erlangen", 37711);

        	//Add Flying spells:
        	target->learnSpell(34090, false); //Ride 225
        	target->learnSpell(54197, false); //Cold weather flying

        	//Increase Account transfer count:
        	SQLTransaction trans = LoginDatabase.BeginTransaction();

        	//Add enough money to learn class skills:
        	uint32 targetMoney = target->GetMoney();
        	int32 newmoney = int32(targetMoney) + 15000000;

        	if (newmoney > MAX_MONEY_AMOUNT)
                    newmoney = MAX_MONEY_AMOUNT;

            target->SetMoney(newmoney);
            handler->PSendSysMessage("Euch wurden 1500 Gold zum erlernen aller Fähigkeiten gutgeschrieben!");

            //now, update transfer count
        	PreparedStatement* inc_stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_TRANSFER_COUNT);
			inc_stmt->setUInt32(0, accountId);
			trans->Append(inc_stmt);
        	//Mark transfer as done:
        	PreparedStatement* mark_stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_TRANSFER_AS_DONE);
			mark_stmt->setUInt32(0, transfer_number);
			trans->Append(mark_stmt);

			LoginDatabase.CommitTransaction(trans);

        	vote_functions::NotifyPlayer(target, "Transfer erfolgreich abgeschlossen!");
		}		
		else
		{
			handler->SendSysMessage("Transfer konnte nicht gefunden werden!");
			handler->SendSysMessage("Bitte gib einen gültigen Transfer an!");
		}
		return true; 
	}

private:

	static std::string getStatusToCode(uint8 status_code)
	{
		switch(status_code)
		{
			case STATUS_OPEN:
				return "offen";
				break;

			case STATUS_APPROVED:
				return "bestätigt";
				break;

			case STATUS_DONE:
				return "fertig";
				break;

			case STATUS_DECLINED:
				return "abgelehnt";
				break;
		}

	}

	static void AddPlayerSuitableWeaponSkills(Player* target)
	{
		//Set defense Skill to maximum
		target->SetSkill(SKILL_DEFENSE, target->GetSkillStep(SKILL_DEFENSE), 400, target->GetPureMaxSkillValue(SKILL_DEFENSE));
		switch(target->getClass())
		{
			case CLASS_PALADIN:
			case CLASS_WARRIOR:
			case CLASS_DEATH_KNIGHT:
				target->SetSkill(SKILL_2H_SWORDS, target->GetSkillStep(SKILL_2H_SWORDS), 400, target->GetPureMaxSkillValue(SKILL_2H_SWORDS));
				target->SetSkill(SKILL_SWORDS, target->GetSkillStep(SKILL_SWORDS), 400, target->GetPureMaxSkillValue(SKILL_SWORDS));
				//1h und 2h schwerter
				break;
			case CLASS_ROGUE:
				target->SetSkill(SKILL_SWORDS, target->GetSkillStep(SKILL_SWORDS), 400, target->GetPureMaxSkillValue(SKILL_SWORDS));
				target->SetSkill(SKILL_DAGGERS, target->GetSkillStep(SKILL_DAGGERS), 400, target->GetPureMaxSkillValue(SKILL_DAGGERS));
				//dolche und 1h schwerter
				break;
			case CLASS_HUNTER:
				target->SetSkill(SKILL_GUNS, target->GetSkillStep(SKILL_GUNS), 400, target->GetPureMaxSkillValue(SKILL_GUNS));
				target->SetSkill(SKILL_2H_AXES, target->GetSkillStep(SKILL_2H_AXES), 400, target->GetPureMaxSkillValue(SKILL_2H_AXES));
				//schusswaffen + 2h axt
				break;
			case CLASS_DRUID:
				target->SetSkill(SKILL_STAVES, target->GetSkillStep(SKILL_STAVES), 400, target->GetPureMaxSkillValue(SKILL_STAVES));
				target->SetSkill(SKILL_POLEARMS, target->GetSkillStep(SKILL_POLEARMS), 400, target->GetPureMaxSkillValue(SKILL_POLEARMS));
				//stäbe und stangenwaffen
				break;
			case CLASS_SHAMAN:
				target->SetSkill(SKILL_MACES, target->GetSkillStep(SKILL_MACES), 400, target->GetPureMaxSkillValue(SKILL_MACES));
				target->SetSkill(SKILL_FIST_WEAPONS, target->GetSkillStep(SKILL_FIST_WEAPONS), 400, target->GetPureMaxSkillValue(SKILL_FIST_WEAPONS));
				//1h streitkolben und faustwaffen
				break;
			case CLASS_WARLOCK:
			case CLASS_MAGE:
				target->SetSkill(SKILL_STAVES, target->GetSkillStep(SKILL_STAVES), 400, target->GetPureMaxSkillValue(SKILL_STAVES));
				target->SetSkill(SKILL_SWORDS, target->GetSkillStep(SKILL_SWORDS), 400, target->GetPureMaxSkillValue(SKILL_SWORDS));
				//stab und 1h schwert
				break;
			case CLASS_PRIEST:
				target->SetSkill(SKILL_STAVES, target->GetSkillStep(SKILL_STAVES), 400, target->GetPureMaxSkillValue(SKILL_STAVES));
				target->SetSkill(SKILL_MACES, target->GetSkillStep(SKILL_MACES), 400, target->GetPureMaxSkillValue(SKILL_MACES));
				//stab + 1h streitkolben	
				break;		
		}
	}

	static uint32 getSkilltoLearningSpell(uint32 learning_spell)
	{
		switch(learning_spell)
		{
			case 50310:
				return SKILL_MINING;
				break;
			case 50305:
				return SKILL_SKINNING;
				break;
			case 50300:
				return SKILL_HERBALISM;
				break;
			case 51304:
				return SKILL_ALCHEMY;
				break;
			case 51300:
				return SKILL_BLACKSMITHING;
				break;
			case 51309:
				return SKILL_TAILORING;
				break;
			case 51302:
				return SKILL_LEATHERWORKING;
				break;
			case 51306:
				return SKILL_ENGINEERING;
				break;
			case 45363:
				return SKILL_INSCRIPTION;
				break;
			case 51311:
				return SKILL_JEWELCRAFTING;
				break;
			case 51313:
				return SKILL_ENCHANTING;
				break;
			default:
				return 0;
				break;
		}
	}

	static void unlearnAllProfessions(Player* p)
	{
		uint32 profession_list[] = {SKILL_BLACKSMITHING, SKILL_LEATHERWORKING, SKILL_ALCHEMY, SKILL_HERBALISM, SKILL_MINING, SKILL_TAILORING, SKILL_ENGINEERING, SKILL_ENCHANTING, SKILL_SKINNING, SKILL_JEWELCRAFTING, SKILL_INSCRIPTION};
		
		for (int i = 0; i < 11; i++)
		{
			if(p->HasSkill(profession_list[i]))
				p->SetSkill(profession_list[i], 0, 0, 0);			
		}
	}
};

void AddSC_transfer_commands()
{
	new transfer_commands();
}