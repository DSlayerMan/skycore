#include "ScriptPCH.h"
#include "vote.h"


#define GOSSIP_MENU_BACK 	42
#define GOSSIP_MENU_EXIT	43
#define GOSSIP_MENU_BUY		44
#define GOSSIP_MENU_ITEMS	45
#define GOSSIP_MENU_RELOAD  46
#define GOSSIP_MENU_VOTEPOINT 47
#define GOSSIP_MENU_PAYOUT	48
#define BUY_WARNING_POPUP "Wollt ihr diesen Gegenstand wirklich kaufen? \r\n Dieser wird euch dann per Post zugestellt. \r\n\r\n"

class npc_voteshop : public CreatureScript
{
public:
	npc_voteshop(): CreatureScript("npc_voteshop")
	{
		initialized = false;
	}

	std::vector<voteshop_category> categories;
	std::vector<voteshop_item> items;
	bool initialized;

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();

		if(!initialized)
			this->BuildCategories();
		//Test if empty
		if(categories.empty() || items.empty())
		{
			vote_functions::NotifyPlayer(player, "Der Voteshop ist zurzeit deaktiviert");
			return false; //Disallow Gossip when infight
		}
		std::ostringstream category_text;
		//If not, list categories and count:
		for(std::vector<voteshop_category>::iterator it = categories.begin(); it != categories.end(); ++it)
		{
			if(it->disabled)
				continue;
			category_text.str("");
			category_text.clear();
			category_text << it->title << " (" << it->cost << " Votingabzeichen)";
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, category_text.str().c_str(), GOSSIP_MENU_ITEMS, it->index);
		}
		//Get votepoints from webapp:
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Ich möchte Votingabzeichen aus meinen Votes erhalten", GOSSIP_MENU_VOTEPOINT, GOSSIP_ACTION_INFO_DEF);
		//"let me be"-menu:
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Nein danke, ich brauche nichts", GOSSIP_MENU_EXIT, GOSSIP_ACTION_INFO_DEF);
		

		if(player->GetSession()->GetSecurity() != SEC_PLAYER)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "<GM> Reload VoteShop <GM>", GOSSIP_MENU_RELOAD, GOSSIP_ACTION_INFO_DEF);

		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 actions)
	{
		player->PlayerTalkClass->ClearMenus();
		//First: test other Options:
		if(sender == GOSSIP_MENU_EXIT)
		{
			player->PlayerTalkClass->SendCloseGossip();
		}
		else if (sender == GOSSIP_MENU_BACK)
		{
			OnGossipHello(player, creature);
		}
		else if(sender == GOSSIP_MENU_BUY)//Checkout-page!
		{
			uint32 price = getItemPricebyEntry(actions);
			if(vote_functions::TakeVoteItems(price, player))
            {
            	std::string subject = "Deine Waren";
            	std::string text = "Vielen Dank für deine Unterstützung \r\n Dein Lemniscate-WoW Team";
            	vote_functions::SendItemviaMail(player, subject, text, actions);
            	vote_functions::NotifyPlayer(player, "Deine Bestellung wird dir per Post zugestellt.");   	
            }
            else
            {
            	vote_functions::NotifyPlayer(player, "Ihr habt nicht genug Votingabzeichen dabei, um euch das zu kaufen!");
            }
			OnGossipHello(player, creature);
		}
		else if(sender == GOSSIP_MENU_VOTEPOINT)
		{
			PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_VOTEPOINTS_POINTS);
			stmt->setUInt32(0, player->GetSession()->GetAccountId());
			PreparedQueryResult result = LoginDatabase.Query(stmt);
			if(!result)
			{
				OnGossipSelect(player, creature, GOSSIP_MENU_PAYOUT, 0);
				return true;
			}
			//Get result row:
			Field* fields = result->Fetch();
			uint32 player_votes= fields[0].GetUInt32();
			uint32 player_votepoints = (uint32) floor(player_votes / 3);

			if(player_votepoints == 0)
			{
				OnGossipSelect(player, creature, GOSSIP_MENU_PAYOUT, 0);
				return true; 
			}

			//Now: Player has voted and wants his points:

			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Zahlt mir ein Votingabzeichen aus!", GOSSIP_MENU_PAYOUT, 1);

			if(player_votepoints >= 5)
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Zahlt mir fünf Votingabzeichen aus!", GOSSIP_MENU_PAYOUT, 5);
			if(player_votepoints >= 10)
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Zahlt mir zehn Votingabzeichen aus!", GOSSIP_MENU_PAYOUT, 10);
			if(player_votepoints >= 25)
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Zahlt mir 25 Votingabzeichen aus!", GOSSIP_MENU_PAYOUT, 25);
			if(player_votepoints >= 50)
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Zahlt mir 50 Votingabzeichen aus!", GOSSIP_MENU_PAYOUT, 50);

			std::ostringstream all;
    		all << "Zahlt mir alle " << player_votepoints << " Votingabzeichen aus";
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, all.str().c_str(), GOSSIP_MENU_PAYOUT, player_votepoints);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		}
		else if(sender == GOSSIP_MENU_PAYOUT)
		{
			if(actions == 0)
			{
				vote_functions::NotifyPlayer(player, "Ihr habt noch nicht gevotet! Votet online für uns, um euch Votingabzeichen auszahlen zu lassen!");
				player->PlayerTalkClass->SendCloseGossip();
				return true;
			}
			else
			{
				if(player->AddItem(ITEM_VOTEPOINTS, actions))
				{
					std::ostringstream msg;
					uint32 delta_votes = 3 * actions;
    				msg << "Euch wurden " << actions << " Votingabzeichen gegeben und "<< delta_votes <<" Votes abgezogen!";
    				vote_functions::NotifyPlayer(player, msg.str().c_str());
    				//Now: save this:
    				player->SaveforAntiRollback(false);
    				//And change it in the db:
    				PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_VOTEPOINTS_DIFF);
    				stmt->setUInt32(0, actions*3);
					stmt->setUInt32(1, player->GetSession()->GetAccountId());
					LoginDatabase.Execute(stmt);
					player->PlayerTalkClass->SendCloseGossip();
				}
				else
				{
					vote_functions::NotifyPlayer(player, "Euer Inventar ist voll!");
					OnGossipSelect(player, creature, GOSSIP_MENU_VOTEPOINT, 0);
				}
			}
		}
		else if(sender == GOSSIP_MENU_RELOAD)
		{
			this->BuildCategories();
			vote_functions::NotifyPlayer(player, "VoteShop reloaded!");
			OnGossipHello(player, creature);

		}
		else if(sender == GOSSIP_MENU_ITEMS)//Item-page!
		{
			uint32 price =  getCategoryCost(actions);
			for(std::vector<voteshop_item>::iterator it = items.begin(); it != items.end(); ++it)
			{
				if(it->category_id == actions)
				{
					std::string itemname = vote_functions::GetItemName(it->item, player->GetSession());
					player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, itemname, GOSSIP_MENU_BUY, it->item->ItemId, makePopupQuestion(itemname, price), 0, false);
				}
			}
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Zurück", GOSSIP_MENU_BACK, GOSSIP_ACTION_INFO_DEF);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		}
		else
		{
			vote_functions::NotifyPlayer(player, "Ein Fehler trat auf: Code 1");
		}
		return true;

	}

    void BuildCategories()
    {
    	//First: Clear all vectors:
		categories.clear();
		items.clear();

    	PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_VOTE_CATEGORY);
    	PreparedQueryResult result = WorldDatabase.Query(stmt);

    	if (!result)
        	return;
        //First: Build Categories:
    	do
    	{
    	    Field* fields = result->Fetch();
	   	    uint32 category_id = fields[0].GetUInt32();
    	    std::string category_text = fields[1].GetString();
    	    bool disabled = fields[2].GetBool();
    	    uint32 cost = fields[3].GetUInt32();
			//Now: Append the category into the vector:
			categories.push_back(voteshop_category(category_id, category_text, disabled, cost));
    	} while (result->NextRow());

    	//Delete old query + result:

    	stmt = NULL;
    	result = NULL;

    	//Now: read all items and their categories:
    	stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_VOTE_ITEMS_ALL);
    	result = WorldDatabase.Query(stmt);

    	if (!result)
        	return;
        
        do
    	{
    	    Field* fields = result->Fetch();
	   	    uint32 item_entry = fields[0].GetUInt32();
    	    uint32 category_id = fields[1].GetUInt32();
			items.push_back(voteshop_item(sObjectMgr->GetItemTemplate(item_entry),category_id));
    	} while (result->NextRow());
    	
    	
    }

    uint32 getCategoryCost(uint32 category_index)
    {
    	for(std::vector<voteshop_category>::iterator it = categories.begin(); it != categories.end(); ++it)
		{
			if(it->index == category_index)
				return it->cost;
		}
		return 0;
    }

    uint32 getItemPricebyEntry(uint32 entry)
    {
    	for(std::vector<voteshop_item>::iterator it = items.begin(); it != items.end(); ++it)
		{
			if(it->item->ItemId == entry)
			{
				return getCategoryCost(it->category_id);
			}
		}
		return 0;
    }

    const char* makePopupQuestion(std::string itemname, uint32 cost)
    {
    	std::ostringstream text_assembly;
    	text_assembly << BUY_WARNING_POPUP << itemname << "\r\n\r\n" << cost << " Votingabzeichen";
    	const char* c_string = text_assembly.str().c_str();
    	//Clean access:
    	text_assembly.str("");
    	text_assembly.clear();
    	return c_string;
    }
};

void AddSC_npc_voteshop()
{
	new npc_voteshop;
}
