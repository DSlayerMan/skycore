//Script for enabling ingame commands for players to report bugs more easily

#include "ScriptPCH.h"
#include "Player.h"


class bug_commands : public CommandScript
{
public:
	bug_commands() : CommandScript("bug_commands") {}

	ChatCommand* GetCommands() const 
	{
		static ChatCommand bugCommandTable[]=
		{
			{"bug_npc", SEC_PLAYER, false, &HandleBug_NPCCommand,"",NULL},
			{"bug_quest", SEC_PLAYER, false, &HandleBug_QuestCommand,"",NULL},
			{"bug_ort", SEC_PLAYER, false, &HandleBug_OrtCommand, "", NULL},
			{NULL,0,false,NULL,"",NULL}
		};
		return bugCommandTable;
	}

	static bool HandleBug_NPCCommand(ChatHandler* handler, const char* args)
	{
		Creature* target = handler->getSelectedCreature();

        if (!target)
        {
            handler->SendSysMessage("Bitte wählt einen NPC aus!");
            handler->SetSentErrorMessage(true);
            return false;
        }
        handler->PSendSysMessage("Ihr habt folgenden npc ausgewählt: %s", target->GetName().c_str());
        handler->SendSysMessage("Bitte gebt in eurem Bugreport folgende Werte mit an:");
        handler->PSendSysMessage("Typ der Kreatur: %u", target->GetEntry());
		handler->PSendSysMessage("ID der Kreatur: %u", target->GetGUIDLow());
		handler->PSendSysMessage("Sichtbare Welten: %u", target->GetPhaseMask());
		handler->PSendSysMessage("Steuerung der Kreatur: %s / %s", target->GetAIName().c_str(), target->GetScriptName().c_str());

		return true; 
	}
	static bool HandleBug_QuestCommand(ChatHandler* handler, const char* args)
	{
		Player *player = handler->GetSession()->GetPlayer();
		handler->SendSysMessage("Quest Bug Analyse");
		handler->SendSysMessage("Ihr befindet euch auf folgenden Quests:");
		uint8 questcount = 1;
		for (uint8 i = 0; i < MAX_QUEST_LOG_SIZE; ++i)
		{
    		uint32 questid = player->GetQuestSlotQuestId(i);
    		if (questid == 0)
        		continue;
    		const Quest* qtemplate = sObjectMgr->GetQuestTemplate(questid);
    		handler->SendSysMessage((questid + ": " + qtemplate->GetTitle()).c_str());
    		questcount++;
		}
		handler->PSendSysMessage("Es wurden %u Quests gefunden", questcount);
		handler->SendSysMessage("Vielen Dank für deinen Bugreport. Vergiss bitte nicht Informationen zum Ort anzugeben. Diese kannst du dir mit '.bug_ort' anzeigen lassen.");
		return true;
	}
	static bool HandleBug_OrtCommand(ChatHandler* handler, const char* args)
	{
		Player *p = handler->GetSession()->GetPlayer();
		handler->PSendSysMessage("Ihr befindet euch gerade an folgender Position:");
		handler->PSendSysMessage("X-Koordinate: %.0f", p->GetPositionX());
		handler->PSendSysMessage("Y-Koordinate: %.0f", p->GetPositionY());
		handler->PSendSysMessage("Z-Koordinate: %.0f", p->GetPositionZ());
		handler->PSendSysMessage("Orientierung: %.0f", p->GetOrientation());
		handler->PSendSysMessage("Karte: %d -> %s", p->GetMap()->GetId(),p->GetMap()->GetMapName());
		return true;
	}

};

void AddSC_bug_commands()
{
	new bug_commands();
}