//Constants/Classes for vote functionality

#ifndef VOTE_H
#define VOTE_H

#define ITEM_VOTEPOINTS 45978

class vote_functions
{
public:
	vote_functions();
	~vote_functions();
	static void SendRejectMSG(Player* player);
	static void SendAcceptMSG(Player* player);
	static void NotifyPlayer(Player* player, const char *str);
	static bool TakeVoteItems(uint32 count, Player* player);
	static void GiveVoteItems(uint32 count, Player* player);
	static std::string GetItemName(const ItemTemplate* itemTemplate, WorldSession* session);
	static bool SendItemviaMail(Player* player, std::string subject, std::string text, uint32 entry);
	static bool IsHeroic(const ItemTemplate* itemplate);
};

class voteshop_category
{
public:
	voteshop_category(uint32 newIndex, std::string newTitle, bool ifdisabled, uint32 newcost)
	{
		this->index = newIndex;
		this->title = newTitle;
		this->disabled = ifdisabled;
		this->cost = newcost;
	};
	~voteshop_category(){};
	uint32 index;
	std::string title;
	bool disabled;
	uint32 cost;
};

class voteshop_item
{
public:
	voteshop_item(const ItemTemplate* newitem, uint32 newcategory)
	{
		this->item = newitem;
		this->category_id = newcategory;
	};
	~voteshop_item(){};
	const ItemTemplate* item;
	uint32 category_id;
};

#endif