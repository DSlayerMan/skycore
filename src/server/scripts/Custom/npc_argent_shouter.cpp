#include "ScriptPCH.h"

class npc_argent_shouter : public CreatureScript
{
public:
	npc_argent_shouter() : CreatureScript ("npc_argent_shouter"){}

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_argent_shouterAI(creature);
	}

	struct npc_argent_shouterAI: public ScriptedAI
	{
		npc_argent_shouterAI(Creature* creature) : ScriptedAI(creature) 
		{
			sentences[1] = std::string("Tapfere Recken, das Argentumturnier hat seine Zelte in Eiskrone aufgeschlagen");
			sentences[2] = std::string("Messt euch mit anderen, um siegreich aus dem Kampf hervorzugehen");
			sentences[3] = std::string("Tirion Fordring sucht tapfere Streiter, um gegen den Lichkönig anzutreten. Seid ihr mutig genug?");
		}

		//VARIABLEN
		std::string sentences[3];
		uint32 delay;

		void Reset() OVERRIDE
		{
			delay = 600000;
		}

		void UpdateAI(uint32 uiDiff) OVERRIDE
		{
			if(!UpdateVictim())
			{
				if(delay <= uiDiff)
				{
					uint8 r = rand()%3;
					me->MonsterSay((sentences[r]).c_str(), LANG_UNIVERSAL, 0);
					delay = 600000;
				}
				else
					delay -= uiDiff;	
			}
			DoMeleeAttackIfReady();
		}

	};

};

void AddSC_npc_argent_shouter()
{
	new npc_argent_shouter();
}