/*
* Vendor for TrinityCore Transmogrification
* (c) 2013 Leupi
*/
#include "ScriptPCH.h"
#include "transmogrification.h"

#define GOSSIP_MENU_TRANSMOG 	40
#define GOSSIP_MENU_REMOVE_ALL 	41
#define GOSSIP_MENU_REMOVE_ONE  42
#define GOSSIP_MENU_RELOAD		43

#define TRANSMOG_WARNING_POPUP "Wenn ihr diese Gegenstände transmogrifiziert werden sie an euch gebunden und nicht mehr handelbar sein!\r\n Möchtet ihr fortfahren?\r\n\r\n"
#define TRANSMOG_WARNING_NEWLINE "\r\n\r\n|cffffffffzu\r\n\r\n"
#define TRANSMOG_WARNING_REMOVE_ONE "Wollt ihr die Transmogrifikation von diesem Gegenstand wirklich entfernen?\r\n\r\n%s\r\n"
#define TRANSMOG_WARNING_REMOVE_ALL "Wollt ihr wirklich alle eure Transmogrifikationen entfernen?\r\n"

class npc_transmogrifier : public CreatureScript
{
	public:
		npc_transmogrifier(): CreatureScript("npc_transmogrifier"){}

	bool OnGossipHello(Player* player, Creature* creature)
	{
        player->PlayerTalkClass->ClearMenus();
        if(!sTransmogrificationConfig->Enabled)
        {
            NotifyPlayer(player, "Dieses Feature ist im Moment deaktiviert!");
            player->PlayerTalkClass->SendCloseGossip();
        }
		for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_TABARD; slot++) // EQUIPMENT_SLOT_END
   		{
       		if (Item* carrier = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
       		{
           		if (sTransmogrificationConfig->QualityAllowed(carrier->GetTemplate()->Quality))
           		{
               		if (const char* slotName = GetSlotName(slot))
               		{
                        if(slot == EQUIPMENT_SLOT_MAINHAND || slot == EQUIPMENT_SLOT_OFFHAND || slot == EQUIPMENT_SLOT_RANGED) //Weapon icon
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, slotName, GOSSIP_MENU_TRANSMOG , slot);
                        else
                  		    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, slotName, GOSSIP_MENU_TRANSMOG , slot);
               		}               			
           		}
       		}
   		}
   		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_INTERACT_1, "Ich möchte alle Transmogrifikationen entfernen", GOSSIP_MENU_REMOVE_ALL, 0, "Wollt ihr wirklich alle Transmogrifikationen entfernen?\n\r", 0, false);		
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Dieses Menü neu laden", GOSSIP_MENU_RELOAD, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 actions)
	{
		WorldSession* session = player->GetSession();
		player->PlayerTalkClass->ClearMenus();
		switch(sender)
		{
			case GOSSIP_MENU_TRANSMOG:
                if(Item* carrier = player->GetItemByPos(INVENTORY_SLOT_BAG_0, actions))
				{
					uint32 lowGUID = player->GetGUIDLow();
					_items[lowGUID].clear();

					uint32 limit = 0;
					uint32 price = carrier->GetTemplate()->SellPrice;
					if(price< 10000)
					{
						price = CalculatePrice(carrier->GetTemplate()->ItemLevel);
					}
					//Now: Search the first bag:
					for (uint8 i = INVENTORY_SLOT_ITEM_START; i < INVENTORY_SLOT_ITEM_END; i++)
                    {
                        if (limit > 26)
                            break;
                        if (Item* mesh = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
                        {
                            uint32 display = mesh->GetTemplate()->DisplayInfoID;
                            if (carrier->ValidforTransmogrification(mesh) == TRMG_SUCCESS)
                            {
                            	limit++;
                            	_items[lowGUID][display] = mesh;
                                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_VENDOR, GetItemName(mesh->GetTemplate(), session), actions, display, TRANSMOG_WARNING_POPUP + GetItemName(carrier->GetTemplate(), session) + TRANSMOG_WARNING_NEWLINE + GetItemName(mesh->GetTemplate(), session), price, false);
                                
                            }
                        }
                    }
                    //Search the other bags:
                    for (uint8 i = INVENTORY_SLOT_BAG_START; i < INVENTORY_SLOT_BAG_END; i++)
                    {
                        if (Bag* bag = player->GetBagByPos(i))
                        {
                            for (uint32 j = 0; j < bag->GetBagSize(); j++)
                            {
                                if (limit > 26)
                                    break;
                                if (Item* mesh = player->GetItemByPos(i, j))
                                {
                                    uint32 display = mesh->GetTemplate()->DisplayInfoID;
                                    if (carrier->ValidforTransmogrification(mesh) == TRMG_SUCCESS)
                                    {
                                        limit++;
                                        _items[lowGUID][display] = mesh;
                                        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_VENDOR, GetItemName(mesh->GetTemplate(), session), actions, display, TRANSMOG_WARNING_POPUP + GetItemName(carrier->GetTemplate(), session) + TRANSMOG_WARNING_NEWLINE + GetItemName(mesh->GetTemplate(), session), price, false);
                                    }
                                }
                            }
                        }
                    }
                  	//Now the initial Bank:
                    for (uint8 i = BANK_SLOT_ITEM_START; i < BANK_SLOT_ITEM_END; i++)
                    {
                        if (limit > 26)
                            break;
                        if (Item* mesh = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
                        {
                            uint32 display = mesh->GetTemplate()->DisplayInfoID;
                            if (carrier->ValidforTransmogrification(mesh) == TRMG_SUCCESS)
                            {
                            	limit++;
                            	_items[lowGUID][display] = mesh;
                                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, GetItemName(mesh->GetTemplate(), session), actions, display, TRANSMOG_WARNING_POPUP + GetItemName(carrier->GetTemplate(), session) + TRANSMOG_WARNING_NEWLINE + GetItemName(mesh->GetTemplate(), session), price, false);
                                
                            }
                        }
                    }
                    //Now the rest of the bank:
                    for (uint8 i = BANK_SLOT_BAG_START; i < BANK_SLOT_BAG_END; i++)
        			{
        				if (limit > 26)
                            break;
            			if (Bag* pBag = player->GetBagByPos(i))
            			{		
                			for (uint32 j = 0; j < pBag->GetBagSize(); j++)
                			{
                				if (Item* mesh = player->GetItemByPos(i, j))
                				{
                					uint32 display = mesh->GetTemplate()->DisplayInfoID;
                					if (carrier->ValidforTransmogrification(mesh) == TRMG_SUCCESS)
                            		{
                            			limit++;
                            			_items[lowGUID][display] = mesh;
                                		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, GetItemName(mesh->GetTemplate(), session), actions, display, TRANSMOG_WARNING_POPUP + GetItemName(carrier->GetTemplate(), session) + TRANSMOG_WARNING_NEWLINE + GetItemName(mesh->GetTemplate(), session), price, false);
                            		}
                				}
                			}
            			}
        			}
        			char removeOnePopup[250];
                    snprintf(removeOnePopup, 250, TRANSMOG_WARNING_REMOVE_ONE , GetItemName(carrier->GetTemplate(), session).c_str());
                    if(limit==0)
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|cffff0000 Keine transmogrifizierbaren Gegenstände gefunden!", GOSSIP_MENU_RELOAD,0);
                    if(carrier->IsTransmogrified())
                        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_INTERACT_1, "Transmogrifizierung entfernen", GOSSIP_MENU_REMOVE_ONE, actions, removeOnePopup, 0, false);
                    
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Zurück", GOSSIP_MENU_RELOAD, 0);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());			

				}
				else
				{
					   NotifyPlayer(player, "Bitte lege einen Gegenstand an dieser Stelle an!");
					   OnGossipHello(player, creature);
				}
                break;

			case GOSSIP_MENU_REMOVE_ALL:
				for (uint8 Slot = EQUIPMENT_SLOT_START; Slot < EQUIPMENT_SLOT_END; Slot++)
                {
                    if (Item* carrier = player->GetItemByPos(INVENTORY_SLOT_BAG_0, Slot))
                    {
                        carrier->RemoveTransmogrification();
                        player->SetVisibleItemSlot(Slot, carrier);
                    }
                }
                OnGossipHello(player,creature);
                NotifyPlayer(player, "Transmogrifikationen wurden entfernt!");
                break;

			case GOSSIP_MENU_REMOVE_ONE:
				if (Item* carrier = player->GetItemByPos(INVENTORY_SLOT_BAG_0, actions))
                {
                    carrier->RemoveTransmogrification();
                    player->SetVisibleItemSlot(actions, carrier);
                    NotifyPlayer(player, "Transmogrifikation wurde entfernt!");
                }
                OnGossipSelect(player, creature, GOSSIP_MENU_TRANSMOG, actions);
                break;

			case GOSSIP_MENU_RELOAD:
				OnGossipHello(player,creature);
                break;

			default: //No action submitted, so we have to transmogrify!
				uint32 lowGUID = player->GetGUIDLow();
				if (Item* carrier = player->GetItemByPos(INVENTORY_SLOT_BAG_0, sender))
				{
					//Calculate the price:
					uint32 price = carrier->GetTemplate()->SellPrice;
					if(price< 10000)
					{
						price = CalculatePrice(carrier->GetTemplate()->ItemLevel);
					}
                	if(_items[lowGUID].find(actions) != _items[lowGUID].end() && _items[lowGUID][actions]->IsInWorld())
                	{
                		Item* mesh = _items[lowGUID][actions];
                		if(carrier->ValidforTransmogrification(mesh) == TRMG_SUCCESS)
                    	{
                    		if(player->GetMoney() > price)
                    		{
                    			player->ModifyMoney(-1* price);
                    			carrier->Transmogrify(mesh);
	                    		carrier->SetNotRefundable(player);
        		            	carrier->SetBinding(true);
                                carrier->ClearSoulboundTradeable(player);
                                mesh->ClearSoulboundTradeable(player);
        		            	mesh->SetNotRefundable(player);
        		            	mesh->SetBinding(true);
                                _items[lowGUID].clear();
                		    	NotifyPlayer(player, "Transmogrifikation war erfolgreich!");
                                OnGossipSelect(player, creature, EQUIPMENT_SLOT_END, sender);
                                return true;
                    		}
                    		else
                    		{
                    			NotifyPlayer(player, "Ihr habt nicht genug Geld!");
                    		}
                    	}
                    	else
                    	{
                    		NotifyPlayer(player, "Diese Gegenstände könnt ihr nicht transmogrifizieren!");
                    	}
                	}
                	else
                	{
                		//Obviously something went really wrong! Do not do anything, but pass script to OnGossipHello
                	}
				}
				else
				{
					NotifyPlayer(player, "Bitte lege einen Gegenstand an dieser Stelle an!");
				}
                OnGossipHello(player,creature);
                break;
		}
        return true;
	}

private:

	std::map<uint32, std::map<uint32, Item*> > _items;

	const char * GetSlotName(uint8 slot)
    {
        switch (slot)
        {
        case EQUIPMENT_SLOT_HEAD      : return "Kopf";
        case EQUIPMENT_SLOT_SHOULDERS : return "Schulter";
        case EQUIPMENT_SLOT_CHEST     : return "Brust";
        case EQUIPMENT_SLOT_WAIST     : return "Taille";
        case EQUIPMENT_SLOT_LEGS      : return "Beine";
        case EQUIPMENT_SLOT_FEET      : return "Füße";
        case EQUIPMENT_SLOT_WRISTS    : return "Hangelenke";
        case EQUIPMENT_SLOT_HANDS     : return "Hände";
        case EQUIPMENT_SLOT_BACK      : return "Rücken";
        case EQUIPMENT_SLOT_MAINHAND  : return "Waffenhand";
        case EQUIPMENT_SLOT_OFFHAND   : return "Schildhand";
        case EQUIPMENT_SLOT_RANGED    : return "Distanzwaffe";
        default: return NULL;
        }
    }

    void NotifyPlayer(Player* player, const char *str)
	{
		ChatHandler(player->GetSession()).SendSysMessage(str);
		player->GetSession()->SendAreaTriggerMessage(str);
	}

	uint32 CalculatePrice(uint32 Itemlevel)
	{
        uint32 price = Itemlevel*Itemlevel+Itemlevel;
		return (price < 10000 ? 10000 : price );
	}

    std::string GetItemName(const ItemTemplate* itemTemplate, WorldSession* session)
   	{
        std::string name = itemTemplate->Name1;
        int loc_idx = session->GetSessionDbLocaleIndex();
        if (loc_idx >= 0)
            if (ItemLocale const* il = sObjectMgr->GetItemLocale(itemTemplate->ItemId))
                sObjectMgr->GetLocaleString(il->Name, loc_idx, name);

        //Colorize Item name:
        std::string color="";
        switch(itemTemplate->Quality)
        {
            case ITEM_QUALITY_UNCOMMON: color="|cff26c426 " ; break;
            case ITEM_QUALITY_RARE: color="|cff2d2de1 " ; break;
            case ITEM_QUALITY_EPIC: color="|cff8c02cd " ; break;
        }
        //Add ItemLvl:
        std::ostringstream ilvl;
        ilvl << color << name; // << " (Stufe: " << itemTemplate->ItemLevel << ")";
        if(IsHeroic(itemTemplate))
            ilvl << " (Heroisch)";
        return ilvl.str();
    }

    bool IsHeroic(const ItemTemplate* itemplate)
    {
        if(itemplate->Flags & 0x8)
            return true;
        else
            return false;
    }
};

void AddSC_npc_transmogrifier()
{
	new npc_transmogrifier();

}
