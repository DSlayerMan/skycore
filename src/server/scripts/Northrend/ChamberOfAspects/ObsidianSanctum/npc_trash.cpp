//Script for Obsidian Sanctum Trash mobs

#include "ScriptPCH.h"

//DEFINES:
#define SPELL_FRENZY				53801
#define SPELL_CURSE_OF_MENDING		39647
#define SPELL_CURSE_OF_MENDING_H	58948
#define SPELL_SHOCKWAVE				57728
#define SPELL_SHOCKWAVE_H			58947
#define SPELL_PUMMEL				58953
#define SPELL_HAMMER_DROP			57759
#define SPELL_AVENGING_FURY			57742
#define SPELL_MORTAL_STRIKE			13737
#define SPELL_DRACONIC_RAGE			57733
#define SPELL_DRACONIC_RAGE_H		58942
#define SPELL_DEVOTION_AURA			57740
#define SPELL_DEVOTION_AURA_H		58944
#define SPELL_FLAME_SHOCK			43303  //not orinal spell, original spell dmg too low
#define SPELL_RAIN_OF_FIRE			57757 
#define SPELL_RAIN_OF_FIRE_H		58936
#define SPELL_CONJURE_FLAME_ORB		57753 //spell is not working

#define NPC_GENERAL 				30680

//SCRIPT START

class npc_Onyx_Sanctum_Guardian : public CreatureScript
{
public:
	 npc_Onyx_Sanctum_Guardian() : CreatureScript("npc_Onyx_Sanctum_Guardian") { }

	 CreatureAI* GetAI(Creature* creature) const OVERRIDE
     {
	 	return new npc_Onyx_Sanctum_GuardianAI(creature);
	 }

	 struct npc_Onyx_Sanctum_GuardianAI : public ScriptedAI
	 {
	 	npc_Onyx_Sanctum_GuardianAI(Creature* creature) : ScriptedAI(creature){}

		uint32 m_FrenzyTimer;
	 	uint32 m_CurseOfMendingTimer;
	 	uint32 m_ShochwaveTimer;
	 	bool m_isFrenzied;

	 	void Reset() OVERRIDE
	 	{
	 		m_FrenzyTimer             = 30000;
	 		m_ShochwaveTimer          = urand(12000, 16000);
	 		m_CurseOfMendingTimer     = 8000;
	 		m_isFrenzied 			  = false;
	 		me->RemoveAura(SPELL_FRENZY);	
	 	}

	 	void UpdateAI(uint32 uiDiff) OVERRIDE
	 	{
	 		if(!UpdateVictim())
	 			return;

	 		if (m_FrenzyTimer <= uiDiff && !m_isFrenzied)
	 		{
	 			DoCast(me, SPELL_FRENZY);
	 			m_isFrenzied = true;
	 		}
	 		else 
	 			m_FrenzyTimer -= uiDiff;

	 		if (m_ShochwaveTimer <= uiDiff)
	 		{
	 			DoCastVictim(RAID_MODE(SPELL_SHOCKWAVE, SPELL_SHOCKWAVE_H));
	 			m_ShochwaveTimer = urand(1200, 16000);
	 		}
	 		else
	 			m_ShochwaveTimer -= uiDiff;

	 		if (m_CurseOfMendingTimer <= uiDiff)
	 		{
	 			if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))              
            		DoCast(target,(RAID_MODE(SPELL_CURSE_OF_MENDING, SPELL_CURSE_OF_MENDING_H)));                
              
            	m_CurseOfMendingTimer = 8000;
	 		}
	 		else
	 			m_CurseOfMendingTimer -= uiDiff;

	 		DoMeleeAttackIfReady();
	 	}
	};
};

class npc_Onyx_Flight_Captain : public CreatureScript
{
public:
	npc_Onyx_Flight_Captain() : CreatureScript("npc_Onyx_Flight_Captain") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
    {
		return new npc_Onyx_Flight_CaptainAI(creature);
	}

	struct npc_Onyx_Flight_CaptainAI : public ScriptedAI
	{
	 	npc_Onyx_Flight_CaptainAI(Creature* creature) : ScriptedAI(creature){}

	 	uint32 m_PummelTimer;
	 	uint32 m_HammerDropTimer;

	 	void Reset() OVERRIDE
	 	{
	 		m_PummelTimer         = 5000;
	 		m_HammerDropTimer     = 15000;
	 	}

	 	void UpdateAI(uint32 uiDiff) OVERRIDE
	 	{
	 		if(!UpdateVictim())
	 			return;

	 		if(GetClosestCreatureWithEntry(me, NPC_GENERAL, 25.0f, true))
	 		{
	 			me->AddAura(SPELL_AVENGING_FURY, me);
	 		}
	 		else
	 		{
	 			if(me->HasAura(SPELL_AVENGING_FURY))
	 				me->RemoveAura(SPELL_AVENGING_FURY);
	 		}

	 		if (m_PummelTimer <= uiDiff)
	 		{
	 			DoCastVictim(SPELL_PUMMEL);
	 			m_PummelTimer = 5000;
	 		}
	 		else 
	 			m_PummelTimer -= uiDiff;

	 		if (m_HammerDropTimer <= uiDiff)
	 		{
	 			DoCastVictim(SPELL_HAMMER_DROP);
	 			m_HammerDropTimer = 15000;
	 		}
	 		else
	 			m_HammerDropTimer -= uiDiff;

	 		DoMeleeAttackIfReady();
	 	}
	};
};

class npc_Onyx_Brood_General : public CreatureScript
{
public:
	npc_Onyx_Brood_General() : CreatureScript("npc_Onyx_Brood_General") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
    {
		return new npc_Onyx_Brood_GeneralAI(creature);
	}

	struct npc_Onyx_Brood_GeneralAI : public ScriptedAI
	{
	 	npc_Onyx_Brood_GeneralAI(Creature* creature) : ScriptedAI(creature){}

	 	uint32 m_DraconicRageTimer;
	 	uint32 m_MortalStrikeTimer;

	 	void Reset() OVERRIDE
	 	{
	 		m_DraconicRageTimer = 30000;
	 		m_MortalStrikeTimer = urand(11000, 15000);
	 	}

	 	void EnterCombat(Unit*) OVERRIDE
	 	{
	 		DoCast(me, (RAID_MODE(SPELL_DEVOTION_AURA, SPELL_DEVOTION_AURA_H)));	
	 		me->AddAura(SPELL_AVENGING_FURY, me);	
	 	}

	 	void UpdateAI(uint32 uiDiff)  OVERRIDE
	 	{
	 		if(!UpdateVictim())
	 			return;

	 		if (m_MortalStrikeTimer <= uiDiff)
	 		{
	 			DoCastVictim(SPELL_MORTAL_STRIKE);
	 			m_MortalStrikeTimer = urand(11000, 15000);
	 		}
	 		else
	 			m_MortalStrikeTimer -= uiDiff;

	 		if (m_DraconicRageTimer <= uiDiff)
	 		{
	 			me->AddAura((RAID_MODE(SPELL_DRACONIC_RAGE, SPELL_DRACONIC_RAGE_H)), me);
	 			//DoCast(me, (RAID_MODE(SPELL_DRACONIC_RAGE, SPELL_DRACONIC_RAGE_H)));
	 			m_DraconicRageTimer = 60000;
	 		}
	 		else
	 			m_DraconicRageTimer -= uiDiff;

	 		DoMeleeAttackIfReady();
	 	}
	};
};

class npc_Onyx_Blaze_Mistress : public CreatureScript
{
public:
	npc_Onyx_Blaze_Mistress() : CreatureScript("npc_Onyx_Blaze_Mistress") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
    {
		return new npc_Onyx_Blaze_MistressAI(creature);
	}

	struct npc_Onyx_Blaze_MistressAI : public ScriptedAI
	{
	 	npc_Onyx_Blaze_MistressAI(Creature* creature) : ScriptedAI(creature){}

	 	uint32 m_FlameShockTimer;
	 	uint32 m_RainofFireTimer;
	 	uint32 m_ConjureFlameOrbTimer;

	 	void Reset() OVERRIDE
	 	{
	 		m_FlameShockTimer       = 6000;
	 		m_RainofFireTimer       = urand(12000, 20000);
	 		m_ConjureFlameOrbTimer  = 20000;
	 	}

	 	void UpdateAI(uint32 uiDiff) OVERRIDE
	 	{
	 		if(!UpdateVictim())
	 			return;

	 		if(GetClosestCreatureWithEntry(me, NPC_GENERAL, 25.0f, true))
	 		{
	 			me->AddAura(SPELL_AVENGING_FURY, me);
	 		}
	 		else
	 		{
	 			if(me->HasAura(SPELL_AVENGING_FURY))
	 				me->RemoveAura(SPELL_AVENGING_FURY);
	 		}

	 		if (m_FlameShockTimer <= uiDiff)
	 		{
	 			if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))              
               		DoCast(target, SPELL_FLAME_SHOCK);
               	m_FlameShockTimer = 6000;	
	 		}
	 		else
	 			m_FlameShockTimer -= uiDiff;

	 		if (m_RainofFireTimer <= uiDiff)
	 		{
	 			if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))              
               		DoCast(target,(RAID_MODE(SPELL_RAIN_OF_FIRE, SPELL_RAIN_OF_FIRE_H)));
               	m_RainofFireTimer = urand(12000, 20000) ;
	 		}
	 		else
	 			m_RainofFireTimer -= uiDiff;

	 		if (m_ConjureFlameOrbTimer <= uiDiff)  //spell isnt working
	 		{
	 			m_ConjureFlameOrbTimer = 20000;
	 		}
	 		else
	 			m_ConjureFlameOrbTimer -= uiDiff;

	 		DoMeleeAttackIfReady();
	 	}
    };
};

void AddSC_npc_ObsidianSanctum_Trash()
{
	new npc_Onyx_Sanctum_Guardian();
	new npc_Onyx_Flight_Captain();
	new npc_Onyx_Brood_General();
	new npc_Onyx_Blaze_Mistress();
}
